/*
package com.myspace.helloworld;

import org.junit.Test;
import org.jbpm.test.JbpmJUnitBaseTestCase;
import org.junit.After;
import org.junit.Before;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.process.ProcessInstance;


public class SampleProcessTest extends JbpmJUnitBaseTestCase {

    private RuntimeManager runtimeManager;

	private RuntimeEngine runtimeEngine;

	private KieSession kieSession;

	public SampleProcessTest() {
		// Setup the datasource and enable persistence.
		super(true, true);
    }

    
    @Test
    public void testProcess() {
        // create runtime manager with single process - Workflow1.bpmn
        runtimeManager = createRuntimeManager("com/myspace/helloworld/Workflow1.bpmn");

        // take RuntimeManager to work with process engine
        RuntimeEngine runtimeEngine = getRuntimeEngine();

        // get access to KieSession instance
        KieSession kieSession = runtimeEngine.getKieSession();
 
        // start process
        ProcessInstance processInstance = kieSession.startProcess("HelloWorld.Workflow1");

        // check whether the process instance has completed successfully
        assertProcessInstanceCompleted(processInstance.getId(), kieSession);

        
    }

    @Test
    public void testProcessVariables() {
        
    }
}
*/